import React, { Component } from 'react';
import './css/signin.css';

class Signin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: ''
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(e) {
		const value = e.target.value;
		const name = e.target.name;
		this.setState((prevState, props) => ({ [name]: value }));
	}

	handleSubmit(e) {
		alert('email : ' + this.state.email + ' password : ' + this.state.password);
		e.preventDefault() // Évite le raffraichissement de la page 

	}

	render() {
		return (
			<div className="container">
				<div className="col-xs-offset-4 col-xs-4 text-center">
					<form className="form-group" onSubmit={this.handleSubmit} >
						<h1>Sign in</h1>
						<input name='email' value={this.state.value} onChange={this.handleChange} className="form-control" placeholder="Enter your email" /> <br />
						<input name='password' type="password" value={this.state.value} onChange={this.handleChange} className="form-control" placeholder="Enter your password"/> <br />
						<button className="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		)
	}
}

export default Signin;
